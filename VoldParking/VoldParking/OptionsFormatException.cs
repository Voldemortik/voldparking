﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoldParking
{
    class OptionsFormatException:ArgumentException
    {
        public OptionsFormatException(string message):base(message)
        {

        }
    }
}
