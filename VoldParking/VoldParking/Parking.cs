﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json;


namespace VoldParking
{
    class Parking
    {
        
        static string path = @"..\..\Transaction.log";
        /// <summary>
        /// //////////
        /// </summary>
        private static Parking instance;
        public static float Balance { get; set; }
        public Dictionary<int, Vehicles> VehicleList { get; set; }
        public Dictionary<int, Timer> TimersList { get; set; }
        private List<Transaction> TransacInMinute { get; set; }
        private static object syncRoot = new object();
        TimerCallback tm = new TimerCallback(TimerTransaction);

        protected Parking(float balance)
        {
            Balance = balance;
        }

        public static Parking getInstance(float balance)
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new Parking(balance);
                }
            }
            return instance;
        }

        public void AddVehiclesToParking(Vehicles vehicle)
        {

            ParkingPlace parkingID = new ParkingPlace(this);
            if (parkingID.vehiclesPlace != -1)
            {
                vehicle.Id = parkingID.vehiclesPlace;
                vehicle.ParkingTime = DateTime.Now;
                VehicleList.Add(parkingID.vehiclesPlace, vehicle);
                TimersList.Add(vehicle.Id, new Timer(tm, vehicle, 0, 5000));

                Console.WriteLine($"Спасибо что выбираете именно нас!");
                Console.WriteLine($"Ваш id ={vehicle.Id}");
                
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Извините, но сейчас нет свободных мест(");
                Console.ReadKey();
            }


        }
        public void DeleteVehiclesFromParking(int vehiclesId)
        {
            if (VehicleList.ContainsKey(vehiclesId))
            {
                Console.Clear();
                if (VehicleList[vehiclesId].Balance > 0)
                {
                    Console.WriteLine($"Возвращаем ваш остаток средств :{VehicleList[vehiclesId].Balance}");
                }
                else
                {
                    Console.WriteLine($"Ваша задолженность составляет :{Math.Abs(VehicleList[vehiclesId].Balance)} (Оплатите при выезде)");
                }
                VehicleList.Remove(vehiclesId);

                TimersList[vehiclesId].Dispose();
                TimersList.Remove(vehiclesId);
                Console.WriteLine($"Ваше Транспортное средство с id({ vehiclesId}) удалено со списка.С нетерпением ждем вас снова!");
                
                Console.ReadKey();
                Console.Clear();
            }
            else
            {
                Console.Clear();
                Console.WriteLine($"Простите, но Транспортное средство с id ({vehiclesId}) НЕ зарегистрировано.Проверьте  правильность ввода!");
                Console.ReadKey();
                Console.Clear();
            }

        }
        public void ShowAllVehicles()
        {
            
            if (VehicleList.Count == 0)
            {
                Console.WriteLine("Пуст");
            }
            foreach (KeyValuePair<int, Vehicles> entry in VehicleList)
            {
                Console.WriteLine(entry.Key + ". " + VehicleList[entry.Key].Name);
                Console.WriteLine("     Дата заезда: " + VehicleList[entry.Key].ParkingTime);
                Console.WriteLine("     Баланс автомобиля: " + VehicleList[entry.Key].Balance);

            }
            Console.WriteLine("(Нажмите ENTER для продолжения )");
            Console.ReadKey();
            Console.Clear();

        }
        public void AddBalanceVehicle(int vehicleId, double refill)
        {
            if (VehicleList.ContainsKey(vehicleId)) {
                VehicleList[vehicleId].Balance += refill;
            }
            else
            {
                Console.WriteLine($"Простите, но авто с id ({vehicleId}) НЕ зарегистрировано.Проверьте  правильность ввода!");
                Console.ReadKey();
                Console.Clear();
            }


        }
        public void ShowBusyFreeParkingPlaces()
        {
            int deltaPlaces = 10 - VehicleList.Count;
            Console.WriteLine($"Кол-во свободных мест : {deltaPlaces}");
            Console.WriteLine($"Кол-во занятых мест  : {VehicleList.Count}");

        }

        static public void TimerTransaction(object vehicle)
        {

            Vehicles car = (Vehicles)vehicle;
            Transaction carTransaction = new Transaction(car);
            string jsonConvert = JsonConvert.SerializeObject(carTransaction);

            try
            {
                using (StreamWriter sw = new StreamWriter(path, true))
                {
                    sw.WriteLine(jsonConvert);
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.Message + "(Нажмите ENTER для продолжения )");
            }

        }
        public void TransactionToConsole()
        {
            int count = 1;
            foreach (Transaction transaction in TransactionFromFile())
            {
                Console.WriteLine($"{count}) {transaction.TransactionTime} ({transaction.VehicleIdentificator.Name}),уплачено: {transaction.payment}");
                count++;
            }

        }
        public void TransactionPerMinute()
        {
            int count = 1;
            DateTime minusMinuteTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute - 1, DateTime.Now.Second);

            foreach (Transaction transaction in TransactionFromFile())
            {
                if (transaction.TransactionTime >= minusMinuteTime)
                {
                    Console.WriteLine($"{count}) {transaction.TransactionTime} ({transaction.VehicleIdentificator.Name}),уплачено: {transaction.payment}");
                    count++;
                }
            }
        }
        public void PaymentsPerMinute()
        {
            double paymentsSum = 0;
            DateTime minusMinuteTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute - 1, DateTime.Now.Second);

            foreach (Transaction transaction in TransactionFromFile())
            {
                if (transaction.TransactionTime >= minusMinuteTime)
                {
                    paymentsSum += transaction.payment;
                }
            }
            Console.WriteLine($"Заработано за минуту: {paymentsSum }");
        }
        public List<Transaction> TransactionFromFile()
        {
            List<Transaction> transactionList = new List<Transaction>();
            try { 
            
            using (StreamReader sr = new StreamReader(path))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    transactionList.Add(JsonConvert.DeserializeObject<Transaction>(line));
                }

              
            }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.Message + "(Нажмите ENTER для продолжения )");
            }

            return transactionList;
        }
    
    

    }
}
