﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoldParking
{
    class Program
    {
        static void Main(string[] args)
        {
            try { 
            VoldPark voldemortParking = new VoldPark();
            voldemortParking.Launch(InitialSettings.defaultParkingBalance);

            int position = -1;
            int vehicleIdentify = -1;
            while (position != 0)
            {
                Console.WriteLine("Welcome to VoldParking (select an option below)");
                Console.WriteLine();
                Console.WriteLine("1. Узнать баланс парковки.");
                Console.WriteLine("2. Сумма заработанных средств за последнюю минуту.");
                Console.WriteLine("3. Узнать количество свободных / занятых мест на парковке.");
                Console.WriteLine("4. Вывести на экран все транзакции парковки за последнюю минуту");
                Console.WriteLine("5. Вывести всю историю Транзакций ");
                Console.WriteLine("6. Вывести на экран список всех транспортных средств");
                Console.WriteLine("7. Поставить Транспортное средство на парковку");
                Console.WriteLine("8. Забрать Транспортное средство с парковки");
                Console.WriteLine("9. Пополнить баланс конкретного Транспортного средства");
                Console.WriteLine("0. Exit");



                    position = Convert.ToInt32(Console.ReadLine());
                switch (position)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine($"Баланс парковки: {Parking.Balance}");
                        Console.WriteLine();
                        break;
                    case 2:
                            Console.Clear();
                            voldemortParking.Vpark.PaymentsPerMinute();
                            Console.WriteLine();
                            break;
                    case 3:
                            Console.Clear();
                            voldemortParking.Vpark.ShowBusyFreeParkingPlaces();
                            Console.WriteLine();
                            break;
                    case 4:

                            Console.Clear();
                            Console.WriteLine("         История транзакций(за последнюю минуту)");
                            Console.WriteLine();
                            voldemortParking.Vpark.TransactionPerMinute();
                            Console.WriteLine("(Нажмите ENTER для продолжения )");
                            Console.ReadLine();
                            Console.Clear();
                            break;
                    case 5:
                            Console.Clear();
                            Console.WriteLine("         История транзакций");
                            Console.WriteLine();
                            voldemortParking.Vpark.TransactionToConsole();
                            Console.WriteLine("(Нажмите ENTER для продолжения )");
                            Console.ReadLine();
                            Console.Clear();
                            break;
                    case 6:
                            Console.Clear();
                            Console.WriteLine("         Список автомобилей");
                            Console.WriteLine();
                            voldemortParking.Vpark.ShowAllVehicles();
                            Console.WriteLine();
                            break;
                    case 7:
                        Console.Clear();

                        int carType = -1;
                        string carName;
                        bool isAdded = false;

                        Vehicles vehicle;
                        Console.WriteLine("Выберите тип вашего транспортного средства");
                        Console.WriteLine();
                        Console.WriteLine("1. Легковая");
                        Console.WriteLine("2. Грузовая");
                        Console.WriteLine("3. Автобус ");
                        Console.WriteLine("4. Мотоцикл");
                        Console.WriteLine("0. В главное меню");
                            carType = Convert.ToInt32(Console.ReadLine());
                            if (carType == 0)
                            {
                                Console.Clear();
                                break;
                            }
                        Console.WriteLine("Укажите имя для идентификации вашего транспортного средства");
                        carName = Console.ReadLine();
                            if (!string.IsNullOrEmpty(carName))
                            {
                                switch (carType)
                                {


                                    case 1:

                                        vehicle = new PassengerСar(carName);
                                        voldemortParking.Vpark.AddVehiclesToParking(vehicle);


                                        break;
                                    case 2:

                                        vehicle = new Truck(carName);
                                        voldemortParking.Vpark.AddVehiclesToParking(vehicle);

                                        break;
                                    case 3:

                                        vehicle = new Bus(carName);
                                        voldemortParking.Vpark.AddVehiclesToParking(vehicle);

                                        break;
                                    case 4:

                                        vehicle = new Motorcycle(carName);
                                        voldemortParking.Vpark.AddVehiclesToParking(vehicle);

                                        break;
                                    case 0:

                                        break;

                                    default:
                                        throw new OptionsFormatException("Неверно указано число!Укажите число из списка!");

                                }
                            }
                            else
                            {
                                Console.WriteLine("Неверный идентификатор:было указано пустую строку.");
                            }
                            Console.ReadKey();
                            Console.Clear();

                            break;
                    case 8:
                            Console.Clear();
                            Console.Write("Укажите ваш Id: ");
                        vehicleIdentify = Convert.ToInt32(Console.ReadLine());
                        voldemortParking.Vpark.DeleteVehiclesFromParking(vehicleIdentify);


                        break;
                    case 9:
                            Console.Clear();
                            double vehicleRefill = 0;
                        Console.Write("Укажите ваш Id: ");
                        vehicleIdentify = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Укажите сумму пополнения: ");
                        vehicleRefill = Convert.ToDouble(Console.ReadLine());
                        voldemortParking.Vpark.AddBalanceVehicle(vehicleIdentify, vehicleRefill);
                            Console.WriteLine();
                            break;
                       

                    default:
                            Console.Clear();

                            throw new OptionsFormatException("Неверно указано число! Укажите число из списка!");

                        //break;



                }
            }

           }
            catch (OptionsFormatException ex)
            {
                Console.Clear();
                Console.WriteLine(ex.Message + "(Нажмите ENTER для выхода из программы)");
            }
            catch (FormatException ex)
            {
                Console.Clear();
                Console.WriteLine(ex.Message + "Возможно были введены буквы вместо цифр.(Нажмите ENTER для выхода из программы)");
            }

            catch (OverflowException)
            {
                Console.Clear();
                Console.WriteLine("Указано неверное значение.Значение было недопустимо малым или недопустимо большим!" + "Нажмите ENTER для выхода из программы.");

            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine(ex.Message + "(Нажмите ENTER для выхода из программы)");
            }

            Console.ReadLine();
        }
    }
}
