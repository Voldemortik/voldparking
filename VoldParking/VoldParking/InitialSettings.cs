﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoldParking
{
   public static class InitialSettings
    {
        public static float defaultParkingBalance = 0;
        public static int maxParkingQuntity = 10;
        public static float penaltyRatio = 2.5f;
        public static int truckTariff = 5;
        public static int passengerCarTariff = 2;
        public static int motorcycleTariff = 1;
        public static float busTariff = 3.5f;
        public static int withdrawalsDefaultTime = 5;
    }
}


 
    
       